import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

import {
  div,
  Header,
  TopHeader,
  Item,
  Content,
  Navbar,
  Section,
  Card,
  Article
} from './style.js'

class App extends Component {
  constructor(props) {
    super(props)

    this.state = {
      page:1
    }
  }

  componentDidMount() {
    window.onscroll = () => {
      const scrollPosition = document.documentElement.scrollTop || document.body.scrollTop
      const maxScroll = document.documentElement.offsetHeight - window.innerHeight
      if (maxScroll - scrollPosition < window.innerHeight / 3) {
        this.setState({ page:5 })
      } else if (scrollPosition < document.getElementById('home').offsetTop + window.innerHeight / 2) { 
        this.setState({ page:1 })
      } else if (scrollPosition < document.getElementById('lastDesign').offsetTop + window.innerHeight / 2) {
        this.setState({ page: 2 })
      } else if (scrollPosition < document.getElementById('popularDesign').offsetTop + window.innerHeight / 2) {
        this.setState({ page: 3 })
      } else if (scrollPosition < document.getElementById('latestArticle').offsetTop + window.innerHeight / 2) {
        this.setState({ page: 4 })
      }
    }
  }

  goTo = destination => () => {
    console.log(destination)
    window.scrollTo(0,document.getElementById(destination).offsetTop-70)
  }

  render() {
    const { page } = this.state
    return (
      <div>
        <Header>
          <TopHeader>
            <div>
              <img  alt="" src={logo} />
            </div>
            <div>
              <div>Hello, Firstname Lastname</div>
              <div><img alt="" src={logo} /></div>
            </div>
          </TopHeader>
          <Navbar>
            <div>
              <Item active={page===1} onClick={this.goTo('home')}>HOME</Item>
              <Item active={page===2} onClick={this.goTo('lastDesign')}>GALERI</Item>
              <Item active={page===3} onClick={this.goTo('popularDesign')}>CARI DESIGNER</Item>
              <Item active={page===4} onClick={this.goTo('latestArticle')}>ARTIKEL</Item>
              <Item active={page===5} onClick={this.goTo('footer')}>TENTANG KAMI</Item>
            </div>
          </Navbar>
        </Header>
        <Content>
          <Section id="home">
            <div>
              <div>
                <p>JELAJAHI DAN TEMUKAN DESAIN INTERIOR SESUAI DENGAN KEBUTUHAN ANDA</p>
                <button>LIHAT GALLERY</button>
              </div>
            </div>
          </Section>
          <Section id="lastDesign">
            <h4 className="title">DESAIN TERBARU</h4>
            <div>
              <Card>
                <div>
                  <img alt="" src="https://images.unsplash.com/photo-1522771739844-6a9f6d5f14af?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=751&q=80"/>
                </div>
                <div>
                  <div>
                    <button>LIVING ROOM</button>
                    <div>
                      <div>Archi Tektoor</div>
                      <div>2 Jan, 2019</div>
                    </div>
                    <div>Vista Verde Apartment Orchid Duplex</div>
                  </div>
                </div>
              </Card>
              <Card>
                <div>
                  <img alt="" src="https://images.unsplash.com/photo-1524758631624-e2822e304c36?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=750&q=80"/>
                </div>
                <div>
                  <div>
                    <button>LIVING ROOM</button>
                    <div>
                      <div>Archi Tektoor</div>
                      <div>2 Jan, 2019</div>
                    </div>
                    <div>Vista Verde Apartment Orchid Duplex</div>
                  </div>
                </div>
              </Card>
              <Card>
                <div>
                  <img alt="" src="https://images.unsplash.com/photo-1518481852452-9415b262eba4?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=750&q=80"/>
                </div>
                <div>
                  <div>
                    <button>LIVING ROOM</button>
                    <div>
                      <div>Archi Tektoor</div>
                      <div>2 Jan, 2019</div>
                    </div>
                    <div>Vista Verde Apartment Orchid Duplex</div>
                  </div>
                </div>
              </Card>
            </div>
          </Section>
          <Section id="popularDesign">
            <h4 className="title">DESAIN TERPOPULER</h4>
            <div>
              <div>
                <div><img alt="" src={logo} /></div>
                <div>Archi Tektoor</div>
                <button>LIHAT PROFIL</button>
              </div>
              <div>
                <img alt="" src="https://interiordesign.id/wp-content/uploads/2017/08/desain-smart-apartemen-feature-image.jpg" />
              </div>
            </div>
          </Section>
          <Section id="latestArticle">
            <h4 className="title">ARTIKEL TERBARU</h4>
            <div>
              <Article>
                <div>
                  <img alt="" src="https://images.unsplash.com/photo-1518481852452-9415b262eba4?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=750&q=80"/>
                </div>
                <div>
                  <div>
                    <div>
                      KAMERA 4D: MEMBUKA KESEMPATAN BARU
                    </div>
                    <div className="tag">INTERIOR | DIGITAL MARKETING</div>
                    <div>
                      <div>Archi Tektoor</div>
                      <div>2 Jan, 2019</div>
                    </div>
                    <p>
                      PT. Silvermedia Indonesia membawakan sebuah teknologi baru ke Indonesia. Teknologi...
                    </p>
                  </div>
                </div>
              </Article>
              <Article>
                <div>
                  <img alt="" src="https://images.unsplash.com/photo-1518481852452-9415b262eba4?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=750&q=80"/>
                </div>
                <div>
                  <div>
                    <div>
                      KAMERA 4D: MEMBUKA KESEMPATAN BARU
                    </div>
                    <div className="tag">INTERIOR | DIGITAL MARKETING</div>
                    <div>
                      <div>Archi Tektoor</div>
                      <div>2 Jan, 2019</div>
                    </div>
                    <p>
                      PT. Silvermedia Indonesia membawakan sebuah teknologi baru ke Indonesia. Teknologi...
                    </p>
                  </div>
                </div>
              </Article>
              <Article>
                <div>
                  <img alt="" src="https://images.unsplash.com/photo-1518481852452-9415b262eba4?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=750&q=80"/>
                </div>
                <div>
                  <div>
                    <div>
                      KAMERA 4D: MEMBUKA KESEMPATAN BARU
                    </div>
                    <div className="tag">INTERIOR | DIGITAL MARKETING</div>
                    <div>
                      <div>Archi Tektoor</div>
                      <div>2 Jan, 2019</div>
                    </div>
                    <p>
                      PT. Silvermedia Indonesia membawakan sebuah teknologi baru ke Indonesia. Teknologi...
                    </p>
                  </div>
                </div>
              </Article>
            </div>
          </Section>
          <Section id="footer">
            <div>
              <div>
                <h4>TENTANG KAMI</h4>
                <p>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                Sed vitae eros mauris. Aenean ullamcorper neque et felis pulvinar, nec viverra velit maximus.
                Vestibulum tellus lectus, interdum sed neque sit amet, semper ultricies lorem.
                Praesent porttitor augue et erat rutrum consequat. Nullam laoreet interdum venenatis.
                </p>
              </div>

              <div>
                <h4>FOLLOW US</h4>
                <div>
                  <svg id="Capa_1" x="0px" y="0px" viewBox="0 0 512 512">
                    <g>
                      <path d="M256,0C114.609,0,0,114.609,0,256c0,141.391,114.609,256,256,256c141.391,0,256-114.609,256-256   C512,114.609,397.391,0,256,0z M256,472c-119.297,0-216-96.703-216-216S136.703,40,256,40s216,96.703,216,216S375.297,472,256,472z   " fill="#FFFFFF"/>
                      <path d="M316.75,216.812h-44.531v-32.5c0-9.969,10.312-12.281,15.125-12.281c4.781,0,28.767,0,28.767,0v-43.859L283.141,128   c-44.983,0-55.25,32.703-55.25,53.672v35.141H195.25V262h32.641c0,58.016,0,122,0,122h44.328c0,0,0-64.641,0-122h37.656   L316.75,216.812z" fill="#FFFFFF"/>
                    </g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g>
                  </svg>
                  <svg id="Capa_1" x="0px" y="0px" viewBox="0 0 612 612">
                    <g>
                      <g>
                        <path d="M437.219,245.162c0-3.088-0.056-6.148-0.195-9.18c13.214-9.848,24.675-22.171,33.744-36.275    c-12.129,5.453-25.148,9.097-38.835,10.626c13.965-8.596,24.675-22.338,29.738-38.834c-13.075,7.928-27.54,13.603-42.924,16.552    c-12.323-14.021-29.904-22.95-49.35-23.284c-37.332-0.612-67.598,30.934-67.598,70.463c0,5.619,0.584,11.072,1.752,16.329    c-56.22-3.616-106.042-32.881-139.369-77c-5.814,10.571-9.152,22.922-9.152,36.164c0,25.037,11.934,47.291,30.071,60.421    c-11.099-0.5-21.503-3.866-30.627-9.375c0,0.306,0,0.612,0,0.918c0,34.996,23.312,64.316,54.245,71.159    c-5.675,1.613-11.656,2.448-17.804,2.421c-4.367-0.028-8.596-0.501-12.713-1.392c8.596,28.681,33.577,49.628,63.147,50.323    c-23.145,19.194-52.298,30.655-83.955,30.572c-5.453,0-10.849-0.361-16.135-1.029c29.933,20.53,65.456,32.491,103.65,32.491    C369.23,447.261,437.219,339.048,437.219,245.162z" fill="#FFFFFF"/>
                        <path d="M612,306C612,137.004,474.995,0,306,0C137.004,0,0,137.004,0,306c0,168.995,137.004,306,306,306    C474.995,612,612,474.995,612,306z M27.818,306C27.818,152.36,152.36,27.818,306,27.818S584.182,152.36,584.182,306    S459.64,584.182,306,584.182S27.818,459.64,27.818,306z" fill="#FFFFFF"/>
                      </g>
                    </g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g>
                  </svg>
                  <svg id="Layer_1" version="1.1" viewBox="0 0 512 512" fill='#fff'><g>
                    <path d="M258.9,507.2C120.4,507.9,6.6,392.6,9.9,252C12.9,118,124,7,262.3,8.7c136.6,1.7,249.4,115.4,245.8,256   C504.7,398.2,394.7,507.8,258.9,507.2z M40.2,257.9c0.9,122.6,97.9,218.2,214.4,219.9c123.6,1.8,222.8-95.7,223.1-219.5   c0.1-122.7-97.8-218-214.5-220.3C143.1,35.7,41.2,132.3,40.2,257.9z"/>
                    <path d="M413.2,261.3c0,21.8-0.6,43.7,0.1,65.5c1.5,41.9-28.7,76.9-68.6,86.9c-7.8,2-16.1,3.2-24.1,3.3c-42.3,0.3-84.6,0.7-126.9,0   c-35.3-0.6-63.1-15.6-81.9-46.1c-7.2-11.7-10.8-24.7-10.8-38.5c-0.1-47.5-0.5-95,0.1-142.4c0.4-31,15.9-53.9,41.6-70.3   c16-10.2,33.7-14.8,52.6-14.8c41.3-0.1,82.6-0.5,123.9,0.1c36.4,0.5,65.2,15.7,83.9,47.7c6.7,11.5,10.1,24.2,10.1,37.7   C413.1,214,413.2,237.7,413.2,261.3z M124,261.1c0,22.8,0,45.7,0,68.5c0,9.1,1.7,17.9,6,26c13.8,26,36.4,37.8,64.9,38.2   c41.3,0.6,82.7,0.3,124,0c7.1,0,14.3-1.2,21.2-3c27.5-7.4,50.9-31.2,50.1-66.1c-1-44-0.2-88-0.2-132c0-9.7-1.8-18.9-6.5-27.4   c-14.1-25.3-36.6-36.8-64.6-37.2c-41.2-0.6-82.3-0.4-123.5,0c-8.2,0.1-16.6,1.6-24.5,4c-26.9,8.3-48.1,32.2-47,64.4   C124.6,218.1,124,239.6,124,261.1z"/>
                    <path d="M257.1,183c40.7-0.1,73.7,32.4,73.7,72.4c-0.1,39.8-33,72.3-73.3,72.4c-40.6,0.1-73.5-32.3-73.5-72.5   C184,215.4,216.6,183.1,257.1,183z M257.1,206.2c-27.4,0.1-50,22.4-49.9,49.3s22.6,49,50.2,49.1c27.5,0,50.1-22.1,50.2-49   C307.5,228.3,284.8,206.1,257.1,206.2z"/>
                    <path d="M354.8,176.7c0.2,8.5-6.8,16-15.1,16.3c-8.5,0.3-15.8-6.9-15.9-15.8c-0.1-8.7,6.6-16,14.9-16.3   C347.2,160.6,354.6,167.9,354.8,176.7z"/>
                    </g>
                  </svg>
                  <svg id="Capa_1" x="0px" y="0px" viewBox="0 0 235 235">
                    <path d="M117.5,0C52.71,0,0,52.71,0,117.5S52.71,235,117.5,235S235,182.29,235,117.5S182.29,0,117.5,0z M117.5,220  C60.981,220,15,174.019,15,117.5S60.981,15,117.5,15S220,60.981,220,117.5S174.019,220,117.5,220z M95.064,110h48.026v7.5  c0,26.481-21.544,48.025-48.026,48.025c-26.482,0-48.025-21.544-48.025-48.025s21.544-48.025,48.025-48.025  c10.684,0,20.806,3.439,29.271,9.946l-9.142,11.893c-5.82-4.474-12.781-6.839-20.129-6.839c-18.211,0-33.025,14.815-33.025,33.025  s14.815,33.025,33.025,33.025c15.632,0,28.762-10.916,32.167-25.525H95.064V110z M184.755,110h13.075v15h-13.075v13.076h-15V125  h-13.069v-15h13.069V96.924h15V110z" fill="#FFFFFF"/>
                    <g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g>
                  </svg>
                </div>
              </div>
            </div>
          </Section>
        </Content>
      </div>
    );
  }
}

export default App;
