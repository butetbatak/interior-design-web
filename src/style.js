import styled from 'styled-components'

const Header = styled.div`
  height: 60px;
  background: #fff;
  display: flex;
  justify-content: space-between;
  box-shadow: 0 0 5px #000;
  position: fixed;
  flex-direction: column;
  width: 100%;
`
const TopHeader = styled.div`
  height: 40px;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  margin: 0 100px;

  & > div:first-child {
    display: flex;

    & > img {
      width: 50px;
    }
  }

  & > div:nth-child(2) {
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    align-items: center;
    align-items: center;
    width: 190px;

    & > div:first-child {
      font-size: 12px;
    }

    & > div:nth-child(2) {
      display: flex;
      align-items: center;
      justify-content: center;
      width: 40px;
      height: 40px;
      background-color: purple;
      border-radius: 50%;

      & > img {
        width: 40px;
      }
    }
  }
`
const Content = styled.div`
  background: #f3f3f3;
`
const Item = styled.div`
  padding: 5px 15px 2px 15px;
  font-size: 10px;
  cursor: pointer;
  border-bottom: 3px solid ${props => props.active ? '#b58820' : '#272727'};
  color: white;
  transition: 0.3s;
  :hover {
    border-bottom: 3px solid #b58820;
  }
`
const Navbar = styled.div`
  display: flex;
  width: 100%;
  justify-content: center;
  background: #272727;
  align-self: flex-end;
  & > div {
    width: 800px;
    display: flex;
    align-self: flex-end;
  }
`
const Section = styled.div`
  min-height: 75vh;
  padding: 50px 0;
  & > div {
    display: flex;
    justify-content: center;
    align-items: center;
  }
  &#home {
    padding: 0px;
    background:url('assets/img/deborah-cortelazzi-615800-unsplash.jpg');
    background-size: cover;
    height: 100vh;
    & > div {
      background: rgba(0,0,0,0.7);
      width: 100%;
      height: 100%;
      display: flex;
      text-align: center;
      align-items: center;
      justify-content: center;
      & > div {
        display: inline-block;
        width: 800px;
        p {
          margin: 0px;
          font-size: 40px;
          color: white;
        }
      }
    }
  }
  &#lastDesign {
    background-color: 
  }
  &#popularDesign {
    & > div {
      background-color: #272727;
      height: 400px;
      display: flex;
      flex-direction: row;

      & > div:first-child {
        display: flex;
        flex-direction: column;
        width: 450px;
        height: 100%;
        display: flex;
        justify-content: center;
        align-items: center;

        & > div:first-child {
          display: flex;
          justify-content: center;
          align-items: center;
          border-radius: 50%;
          width: 150px;
          height: 150px;
          background: linear-gradient(to right, #865f03 0%, #e8b64f 100%);
          margin-bottom: 15px;

          & img {
            width: 100%;
          }
        }

        & > div:nth-child(2) {
          color: white;
          font-size: 18px;
          margin-bottom: 10px;
        }

        & > div:nth-child(3) {
          padding: 5px 25px;
          background-color: yellow;
          color: #fff;
          width: 100px;
        }
      }

      & > div:nth-child(2) {
        width: 450px;
        height: 100%;
        & > img {
          height: 100%;
          width: 100%;
        }
      }
    }
  }
  &#footer {
    background-color: #4f4f4f;
    min-height: unset;
    display: flex;
    text-align: center;
    justify-content: center;
    color: white;
    & > div {
      width: 800px;
      align-items: unset;
      & > div {
        flex-grow: 1;
        text-align: left;
        font-size: 11px;
        min-width: 50%;
        h4 {
          font-weight: 400;
          font-size: 16px;
        }
      }
      & > div:last-of-type {
        text-align:right;
      }
    }
    & svg {
      width: 30px;
      margin: 5px;
    }
  }

  & button {
    background: linear-gradient(to right, #865f03 0%, #e8b64f 100%);
    border: none;
    padding: 15px;
    font-size: 16px;
    color: #fff;
    font-weight: bold;
  }
   & .title {
     width: 100%;
     text-align: center;
     margin: 0 0 20px 0;
     font-size: 30px;
     font-weight: 400;
   }
`
const Card = styled.div`
  width: 250px;
  height: 320px;
  background-color: white;
  margin: 0 20px;
  box-shadow: 0 0 5px #adadad;
  & > div:first-of-type {
    height: 167px;
    overflow: hidden;
    & > img {
      width: 100%;
    }
  }
  & > div:last-of-type {
    height: 153px;
    text-align: center;
    display: flex;
    align-items: center;
    justify-content: center;
    & > div {
      color: #ababab;
    }
    & > div:last-of-type {
      & > div:first-of-type {
        margin: 3px;
        display: flex;
        align-items: center;
        justify-content: space-around;
        font-size: 10px;
      }
      & > div:last-of-type {
        font-size: 20px;
        font-weight: 500;
        color: black;
      }
    }
    
    button {
      padding: 5px;
      font-size: 10px;
    }
  }
`
const Article = styled.div`
  width: 250px;
  height: 400px;
  background-color: white;
  margin: 0 20px;
  & > div:first-of-type {
    height: 250px;
    overflow: hidden;
    & > img {
      height: 100%;
    }
  }
  & > div:last-of-type {
    height: 130px;
    padding: 10px;
    display: flex;
    & > div {
      color: #ababab;
    }
    & > div:last-of-type {
      & div {
        font-size: 10px;
      }
      & > div:first-of-type {
        font-size: 20px;
        font-weight: 500;
        color: black;
      }
      & > div:last-of-type {
        margin: 3px;
        display: flex;
        align-items: center;
        justify-content: space-around;
        div {
          flex-grow: 1;
        }
      }
      .tag {
        font-size: 10px;
        color: #bc9435
      }
      p {
        margin: 0px;
        font-size: 10px;
      }
    }
    
    button {
      padding: 5px;
      font-size: 10px;
    }
  }
`

export {
  Header,
  TopHeader,
  Content,
  Item,
  Navbar,
  Section,
  Card,
  Article
}
